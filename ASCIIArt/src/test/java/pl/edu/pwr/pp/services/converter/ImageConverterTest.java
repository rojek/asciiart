package pl.edu.pwr.pp.services.converter;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;

import pl.edu.pwr.pp.data.Quality;

public class ImageConverterTest {

	@InjectMocks
	private ImageConverter imageConverter;
	@Mock
	private ImageHighQualityConverter highQualityConverter;
	@Mock
	private ImageLowQualityConverter lowQualityConverter;
	private int[][] intensities;

	@Before
	public void setUp() {
		initMocks(this);

		intensities = new int[1][1];
		given(lowQualityConverter.convert(Matchers.any())).willReturn(new char[1][1]);
		given(highQualityConverter.convert(Matchers.any())).willReturn(new char[1][1]);
	}

	@Test
	public void shouldCallLowQualityConverter() throws Exception {

		// given
		Quality quality = Quality.LOW;

		// when
		imageConverter.intensitiesToAscii(intensities, quality);

		// then
		verify(lowQualityConverter).convert(Matchers.eq(intensities));
	}

	@Test
	public void shouldCallHighQualityConverter() throws Exception {
		// given
		Quality quality = Quality.HIGH;

		// when
		imageConverter.intensitiesToAscii(intensities, quality);

		// then
		verify(highQualityConverter).convert(Matchers.eq(intensities));
	}
}
