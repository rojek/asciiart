package pl.edu.pwr.pp.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import pl.edu.pwr.pp.data.LoadedData;
import pl.edu.pwr.pp.data.Quality;
import pl.edu.pwr.pp.data.ResizeMode;
import pl.edu.pwr.pp.gui.ImagePanel;
import pl.edu.pwr.pp.gui.MainAsciiArtWindow;
import pl.edu.pwr.pp.services.converter.ImageConverter;
import pl.edu.pwr.pp.services.reader.ImageFileReader;

public class ImageServicesDispatcherTest {

	private static final int TEST_VALUE = 1;
	private static final int TEST_HEIGHT = 2;
	private static final int TEST_WIDTH = 2;

	@InjectMocks
	ImageServicesDispatcher dispatcher;

	private @Mock ImageResizer imageResizer;
	private @Mock ImageFileReader imageFileReader;
	private @Mock ImageFileWriter imageFileWriter;
	private @Mock ImageConverter imageConverter;
	private @Mock ImageToGreyscaleConverter imageToGreyscaleConverter;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		MainAsciiArtWindow.imagePanel = new ImagePanel();
	}

	@Test
	public void shouldResize() throws Exception {
		// given
		BufferedImage prevImage = Mockito.mock(BufferedImage.class);
		BufferedImage postImage = getMockedImage();
		MainAsciiArtWindow.setImage(prevImage);
		given(imageResizer.resize(Matchers.eq(prevImage), any(ResizeMode.class))).willReturn(postImage);
		LoadedData.resizeMode = ResizeMode.ORIGINAL;
		LoadedData.resized = false;

		// when
		dispatcher.resize();

		// then
		Mockito.verify(imageResizer).resize(Matchers.eq(prevImage), any(ResizeMode.class));
		assertThat(LoadedData.resized).isTrue();
		assertThat(MainAsciiArtWindow.getImage()).isEqualTo(postImage);
		for (int i = 0; i < TEST_HEIGHT; i++) {
			for (int j = 0; j < TEST_WIDTH; j++) {
				assertThat(LoadedData.loadedImage[i][j] == TEST_VALUE);
			}
		}
	}

	@Test
	public void shouldWriteFile() throws Exception {
		// given

		// when
		dispatcher.writeFile("test");

		// then
		verify(imageConverter).intensitiesToAscii(any(), any(Quality.class));
		verify(imageFileWriter).saveToTxtFile(any(), any(String.class));
	}

	private BufferedImage getMockedImage() {

		BufferedImage img = Mockito.mock(BufferedImage.class);
		WritableRaster mockRaster = Mockito.mock(WritableRaster.class);
		
		given(mockRaster.getSample(anyInt(), anyInt(), anyInt())).willReturn(1);
		
		given(img.getHeight()).willReturn(TEST_HEIGHT);
		given(img.getWidth()).willReturn(TEST_WIDTH);
		given(img.getRaster()).willReturn(mockRaster);
		
		return img;
	}
}
