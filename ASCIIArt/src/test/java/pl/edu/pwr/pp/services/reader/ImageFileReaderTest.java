package pl.edu.pwr.pp.services.reader;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class ImageFileReaderTest {

	@InjectMocks
	private ImageFileReader imageFileReader;
	@Mock
	private ImagePgmFileReader imagePgmFileReader;
	@Mock
	private ImageNonPgmFileReader imageNonPgmFileReader;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldReadFileWithPgmExtension() throws Exception {
		// given
		String pgmFileName = "test.pgm";

		// when
		imageFileReader.readImage(pgmFileName);

		// then
		Mockito.verify(imagePgmFileReader).read(Matchers.any());
		Mockito.verify(imageNonPgmFileReader, Mockito.times(0)).read(Matchers.any());
	}

	@Test
	public void shouldReadFileWithOtherThenPgmExtension() throws Exception {
		// given
		String nonPgmFileName = "test.jpg";

		// when
		imageFileReader.readImage(nonPgmFileName);

		// then
		Mockito.verify(imageNonPgmFileReader).read(Matchers.any());
		Mockito.verify(imagePgmFileReader, Mockito.times(0)).read(Matchers.any());
	}
}
