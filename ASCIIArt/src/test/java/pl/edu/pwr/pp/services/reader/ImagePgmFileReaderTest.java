package pl.edu.pwr.pp.services.reader;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import pl.edu.pwr.pp.data.LoadedData;

@RunWith(JUnitParamsRunner.class)
public class ImagePgmFileReaderTest {
	
	@InjectMocks
	private ImagePgmFileReader imageReader;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldReadSequenceFrom0To255GivenTestImage() throws URISyntaxException {
		// given
		Path pathToFile = Paths.get(ClassLoader.getSystemResource("testImage.pgm").toURI()).toAbsolutePath();
		// when
		try {
			imageReader.read(pathToFile);
		} catch (URISyntaxException e) {
			Assert.fail("Should read the file");
		}
		// then
		int counter = 0;
		for (int[] row : LoadedData.loadedImage) {
			for (int intensity : row) {
				assertThat(intensity, is(equalTo(counter++)));
			}
		}
	}

	public Object[] parametersForShouldThrowWhenFileIsInvalid() {
		return new Object[] { "testImageNoP2.pgm", "testImageNoHash.pgm", "testImageTooManyData.pgm",
				"testImage266Greyscale.pgm" };
	}

	@Test
	@Parameters
	public void shouldThrowWhenFileIsInvalid(String fileName) throws Exception {
		//given
		Path pathToFile = Paths.get(ClassLoader.getSystemResource(fileName).toURI()).toAbsolutePath();
		//when
		try {
			imageReader.read(pathToFile);
		}
		//then
		catch (Exception e){
			return;
		}
		Assert.fail("Should have thrown an exception");
	}

}
