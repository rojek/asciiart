package pl.edu.pwr.pp.services;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.edu.pwr.pp.data.ResizeMode.ORIGINAL;
import static pl.edu.pwr.pp.data.ResizeMode.WIDTH_160;
import static pl.edu.pwr.pp.data.ResizeMode.WIDTH_80;

import java.awt.image.BufferedImage;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

public class ImageResizerTest {

	@InjectMocks
	private ImageResizer imageResizer;

	private BufferedImage image;


	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Path pathToTestImage = Paths.get(ClassLoader.getSystemResource("Size4x3Image.jpg").toURI()).toAbsolutePath();
		image = ImageIO.read(pathToTestImage.toFile());
	}

	@Test
	public void shouldResizeTo160WidthAndProperHeight() throws Exception {
		// given

		// when
		BufferedImage img = imageResizer.resize(image, WIDTH_160);

		// then
		assertThat(img.getHeight()).isEqualTo(120);
		assertThat(img.getWidth()).isEqualTo(160);
	}

	@Test
	public void shouldResizeTo80WidthAndProperHeight() throws Exception {
		// given

		// when
		BufferedImage img = imageResizer.resize(image, WIDTH_80);

		// then
		assertThat(img.getHeight()).isEqualTo(60);
		assertThat(img.getWidth()).isEqualTo(80);
	}

	@Test
	public void shouldNotResizeWhenResizeModeIsOriginal() throws Exception {
		// given

		// when
		BufferedImage img = imageResizer.resize(image, ORIGINAL);

		// then
		assertThat(img).isEqualTo(image);
	}
}
