package pl.edu.pwr.pp.services;

import static java.nio.file.Files.deleteIfExists;
import static java.nio.file.Files.exists;
import static java.nio.file.Paths.get;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.Paths;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ImageFileWriterTest {

	private ImageFileWriter imageFileWriter;

	private String fileName;

	@Before
	public void setUp() {
		this.imageFileWriter = new ImageFileWriter();
		fileName = "randomFileName.txt";
	}

	@Test
	public void shouldExistFileAfterCreation() throws Exception {

		// given
		char[][] ascii = new char[1][1];
		ascii[0][0] = 'a';

		// when
		imageFileWriter.saveToTxtFile(ascii, fileName);

		// then
		assertThat(exists(get(fileName))).isTrue();
	}

	@After
	public void tearDown() throws IOException {
		deleteIfExists(Paths.get(fileName));
	}
}
