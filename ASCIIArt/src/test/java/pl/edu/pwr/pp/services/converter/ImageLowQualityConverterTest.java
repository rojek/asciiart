package pl.edu.pwr.pp.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collection;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

@RunWith(Parameterized.class)
public class ImageLowQualityConverterTest {

	@InjectMocks
	private ImageLowQualityConverter imageConverter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Parameters
	public static Collection<Object[]> data() {
		// @formatter:off
		return Arrays.asList(new Object[][] { { 0, '@' }, { 12, '@' }, { 25, '@' }, { 26, '%' }, { 34, '%' },
				{ 51, '%' }, { 52, '#' }, { 66, '#' }, { 76, '#' }, { 77, '*' }, { 89, '*' }, { 102, '*' },
				{ 103, '+' }, { 115, '+' }, { 127, '+' }, { 128, '=' }, { 142, '=' }, { 153, '=' }, { 154, '-' },
				{ 162, '-' }, { 179, '-' }, { 180, ':' }, { 192, ':' }, { 204, ':' }, { 205, '.' }, { 215, '.' },
				{ 230, '.' }, { 231, ' ' }, { 245, ' ' }, { 255, ' ' } });
		// @formatter:on
	}

	private int input;
	private char expected;

	public ImageLowQualityConverterTest(int input, char expected) {
		this.input = input;
		this.expected = expected;
	}

	@Test
	public void shouldReturnRightCharacterForIntensity() {
		Assertions.assertThat(imageConverter.intensityToAsciiLow(this.input)).isEqualByComparingTo(this.expected);
	}

	@Test
	public void shouldReturn2x2CharArrayGiven2x2IntArray() throws Exception {
		// given
		int[][] intensities = new int[][] { { 1, 1 }, { 1, 1 } };

		// when
		char[][] result = imageConverter.convert(intensities);

		// then
		assertThat(result.length).isEqualTo(intensities.length);
		assertThat(result[0].length).isEqualTo(intensities[0].length);
	}
}
