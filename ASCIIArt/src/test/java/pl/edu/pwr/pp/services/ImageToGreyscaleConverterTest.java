package pl.edu.pwr.pp.services;

import static java.awt.image.BufferedImage.TYPE_BYTE_GRAY;
import static java.awt.image.BufferedImage.TYPE_INT_RGB;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;
import static pl.edu.pwr.pp.services.ImageToGreyscaleConverter.BLUE_MULTIPLIER;
import static pl.edu.pwr.pp.services.ImageToGreyscaleConverter.GREEN_MULTIPLIER;
import static pl.edu.pwr.pp.services.ImageToGreyscaleConverter.RED_MULTIPLIER;

import java.awt.image.BufferedImage;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import pl.edu.pwr.pp.data.LoadedData;

public class ImageToGreyscaleConverterTest {

	private @InjectMocks ImageToGreyscaleConverter imageToGreyscaleConverter;

	private @Mock BufferedImage image;

	private int[][] loadedImage = LoadedData.loadedImage;
	private static final Random random = new Random();

	@Before
	public void setUp(){
		initMocks(this);
		given(image.getType()).willReturn(TYPE_INT_RGB);
		given(image.getHeight()).willReturn(1);
		given(image.getWidth()).willReturn(1);
	}
	
	@Test
	public void shouldNotConvertWhenImageAlreadyIsGreyscale() throws Exception {
		// given
		given(image.getType()).willReturn(TYPE_BYTE_GRAY);

		// when
		int[][] result = imageToGreyscaleConverter.convert(image);

		// then
		assertThat(result).isNull();
	}

	@Test
	public void shouldConvertWhenImageAlreadyIsGreyscale() throws Exception {
		// given
		given(image.getType()).willReturn(TYPE_INT_RGB);
		given(image.getRGB(0, 0)).willReturn(random.nextInt(255));
		
		// when
		int[][] result = imageToGreyscaleConverter.convert(image);

		// then
		assertThat(result).isNotNull();
	}

	@Test
	public void shouldConvertRedToGreyscale() throws Exception {
		// given
		given(image.getRGB(0, 0)).willReturn(0xFF0000);

		// when
		int[][] result = imageToGreyscaleConverter.convert(image);

		// then
		assertThat(result).isNotNull();
		assertThat(result[0][0]).isEqualTo((int) (255 * RED_MULTIPLIER));
	}

	@Test
	public void shouldConvertGreenToGreyscale() throws Exception {
		// given
		given(image.getRGB(0, 0)).willReturn(0x00FF00);

		// when
		int[][] result = imageToGreyscaleConverter.convert(image);

		// then
		assertThat(result).isNotNull();
		assertThat(result[0][0]).isEqualTo((int) (255 * GREEN_MULTIPLIER));
	}

	@Test
	public void shouldConvertBlueToGreyscale() throws Exception {
		// given
		given(image.getRGB(0, 0)).willReturn(0x0000FF);

		// when
		int[][] result = imageToGreyscaleConverter.convert(image);

		// then
		assertThat(result).isNotNull();
		assertThat(result[0][0]).isEqualTo((int) (255 * BLUE_MULTIPLIER));
	}
}
