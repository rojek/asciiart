package pl.edu.pwr.pp.gui;

import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;

import pl.edu.pwr.pp.data.LoadedData;
import pl.edu.pwr.pp.data.ResizeMode;
import pl.edu.pwr.pp.services.ImageServicesDispatcher;

class AsciiResizeChangerButton extends JButton {

	private int counter = 0;
	private ImageServicesDispatcher servicesDispatcher = ImageServicesDispatcher.getInstance();

	AsciiResizeChangerButton() {
		super();
		ResizeMode mode = ResizeMode.ORIGINAL;
		setBounds(23, 70, 119, 23);
		LoadedData.resizeMode = mode;
		setText(mode.getDisplayedName());
		addActionListener(this::switchResizeMode);
		addMouseListener(resizeMouseListener);
		repaint();

	}

	private MouseAdapter resizeMouseListener = new MouseAdapter() {

		@Override
		public void mousePressed(MouseEvent event) {
			super.mousePressed(event);
			if (LoadedData.resizeMode.isAppliable() && event.getButton() == MouseEvent.BUTTON3) {
				servicesDispatcher.resize();
			}
		}
	};

	private void switchResizeMode(ActionEvent l) {
		ResizeMode mode = ResizeMode.values()[counter];
		counter++;
		if (counter == ResizeMode.values().length) {
			counter = 0;
		}
		LoadedData.resizeMode = mode;
		setText(mode.getDisplayedName());
		repaint();
		LoadedData.resized = false;
	}
}
