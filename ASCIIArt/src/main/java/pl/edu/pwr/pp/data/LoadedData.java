package pl.edu.pwr.pp.data;

public class LoadedData {

	public static String fileNameToLoad;
	public static String urlToLoad;
	public static LoadMode loadMode;

	public static int[][] loadedImage;
	public static boolean loadSuccesfull = false;
	public static ImageTypes imageType;
	public static Quality quality;
	public static ResizeMode resizeMode;
	public static boolean resized;

}
