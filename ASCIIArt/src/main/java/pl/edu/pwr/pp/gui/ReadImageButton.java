package pl.edu.pwr.pp.gui;

import javax.swing.JButton;
import javax.swing.JFrame;

class ReadImageButton extends JButton {

	ReadImageButton(JFrame frame) {
		super();
		setText("Wczytaj obraz");
		addActionListener(e -> {
			AsciiArtLoadImageDialog newWin = new AsciiArtLoadImageDialog();
			newWin.setVisible(true);
		});
		setBounds(23, 11, 119, 23);
	}

}
