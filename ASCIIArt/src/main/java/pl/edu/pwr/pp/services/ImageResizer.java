package pl.edu.pwr.pp.services;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import pl.edu.pwr.pp.data.ResizeMode;

class ImageResizer {

	BufferedImage resize(BufferedImage originalImage, ResizeMode resizeMode) {
		
		if (!resizeMode.isAppliable())
			return originalImage;
		int width = resizeMode.getWidth();
		int height = calculateImageHeight(originalImage, width);
		
		Image temp = originalImage.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		BufferedImage resized = new BufferedImage(width, height, originalImage.getType());

		Graphics2D g2d = resized.createGraphics();
		g2d.drawImage(temp, 0, 0, null);
		g2d.dispose();

		return resized;
	}

	private static int calculateImageHeight(BufferedImage originalImage, int width) {

		int originalWidth = originalImage.getWidth();
		int originalHeight = originalImage.getHeight();

		double ratio = (double) originalWidth / originalHeight;
		return (int) (width / ratio);
	}
}
