package pl.edu.pwr.pp.gui;

import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import pl.edu.pwr.pp.data.LoadedData;

class ChooseFileButton extends JButton {

	ChooseFileButton(JRadioButton fileRadioButton, FileTextField fileTextField, JPanel contentPanel) {
		setText("Wybierz plik");
		setBounds(315, 7, 89, 23);
		addActionListener(e -> {
			JFileChooser chooser = new JFileChooser(new File("src/main/resources"));
			int returnVal = chooser.showOpenDialog(contentPanel);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				String absolutePath = chooser.getSelectedFile().getAbsolutePath();
				LoadedData.fileNameToLoad = absolutePath;
				fileRadioButton.doClick();
				fileTextField.setText(absolutePath);
			}
		});
	}
}
