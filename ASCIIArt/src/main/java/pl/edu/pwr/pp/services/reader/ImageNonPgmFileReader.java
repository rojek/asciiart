package pl.edu.pwr.pp.services.reader;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;

import javax.imageio.ImageIO;

class ImageNonPgmFileReader extends AbstractImageFileReader {

	@Override
	BufferedImage read(Path path) throws IOException {
		return ImageIO.read(path.toFile());
	}
}
