package pl.edu.pwr.pp.data;

import java.awt.Toolkit;

public enum ResizeMode {


	WIDTH_80(80, "80 znakow"), WIDTH_160(160, "160 znakow"), WIDTH_SCREEN(
			(int) Toolkit.getDefaultToolkit().getScreenSize().getWidth(),
			"Roz. ekranu"), ORIGINAL(null, "Bez skalowania");

	private Integer width;
	private String displayedName;

	private ResizeMode(Integer width, String displayedName) {
		this.width = width;
		this.displayedName = displayedName;
	}

	public Integer getWidth() {
		return width;
	}

	public String getDisplayedName() {
		return this.displayedName;
	}

	public boolean isAppliable() {
		if (this.equals(ResizeMode.ORIGINAL)) {
			return false;
		}
		return true;
	}

}
