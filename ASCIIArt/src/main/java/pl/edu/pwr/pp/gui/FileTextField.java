package pl.edu.pwr.pp.gui;

import java.awt.TextField;

class FileTextField extends TextField {

	FileTextField() {
		super();
		setEditable(false);
		setBounds(47, 37, 260, 20);
		setColumns(10);
	}
}
