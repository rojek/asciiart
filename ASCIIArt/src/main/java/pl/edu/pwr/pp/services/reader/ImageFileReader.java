package pl.edu.pwr.pp.services.reader;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ImageFileReader {

	private AbstractImageFileReader pgmFileReader = new ImagePgmFileReader();
	private ImageNonPgmFileReader nonPgmFileReader = new ImageNonPgmFileReader();

	public BufferedImage readImage(String fileName) throws URISyntaxException, IOException {
		
		if (fileName.split("\\.")[1].equals("pgm"))
			return pgmFileReader.read(getPathToFile(fileName));

		return nonPgmFileReader.read(getPathToFile(fileName));
	}

	private Path getPathToFile(String fileName) throws URISyntaxException {
		return Paths.get(fileName);
	}
}
