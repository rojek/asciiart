package pl.edu.pwr.pp.gui;

import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

import pl.edu.pwr.pp.data.LoadedData;
import pl.edu.pwr.pp.services.ImageServicesDispatcher;

class SaveImageButton extends JButton {

	private ImageServicesDispatcher imageServicesDispatcher = ImageServicesDispatcher.getInstance();

	SaveImageButton(JFrame frame) {
		setText("Zapisz do pliku");
		setEnabled(false);
		setBounds(23, 106, 119, 23);
		addActionListener(e -> {
			JFileChooser fileChooser = new JFileChooser(new File("src/main/resources"));
			int returnVal = fileChooser.showOpenDialog(frame);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				String absolutePath = fileChooser.getSelectedFile().getAbsolutePath();
				try {
					if(!LoadedData.resized){
						imageServicesDispatcher.resize();
					}
					imageServicesDispatcher.writeFile(absolutePath);

				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
	}
}
