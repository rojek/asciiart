package pl.edu.pwr.pp.services.converter;

abstract class AbstractImageQualityConverter {

	abstract char[][] convert(int[][] intensities);
}
