package pl.edu.pwr.pp.gui;

import javax.swing.JButton;
import javax.swing.JFrame;

class CloseButton extends JButton {

	private static final long serialVersionUID = 3372652453482620802L;

	CloseButton(JFrame frame) {
		super();
		setText("Zakończ");
		setBounds(23, 205, 119, 23);
		addActionListener(e -> frame.dispose());
	}
}
