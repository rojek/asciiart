package pl.edu.pwr.pp.gui;

import java.awt.event.ActionEvent;

import javax.swing.JButton;

import pl.edu.pwr.pp.data.LoadedData;
import pl.edu.pwr.pp.data.Quality;

class AsciiQualityChangerButton extends JButton {

	private boolean isLowQuality = false;

	AsciiQualityChangerButton() {
		super();
		setBounds(23, 45, 119, 23);
		setLowQuality();
		addActionListener(this::switchQuality);
	}

	private void setLowQuality() {
		LoadedData.quality = Quality.LOW;
		setText("Niska");
		isLowQuality = true;
	}

	private void setHighQuality() {
		LoadedData.quality = Quality.HIGH;
		setText("Wysoka");
		isLowQuality = false;
	}

	public void switchQuality(ActionEvent e) {
		if (isLowQuality) {
			setHighQuality();
		} else {
			setLowQuality();
		}
		repaint();
	}
}
