package pl.edu.pwr.pp.gui;

import java.awt.TextField;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JRadioButton;

class UrlTextField extends TextField {

	UrlTextField(JRadioButton urlRadioButton) {
		super();
		addFocusListener(new FocusListener() {

			@Override
			public void focusGained(FocusEvent arg0) {
				urlRadioButton.doClick();
			}

			@Override
			public void focusLost(FocusEvent arg0) {
				urlRadioButton.setSelected(false);
			}

		});
		setColumns(10);
		setBounds(50, 88, 260, 20);
	}
}
