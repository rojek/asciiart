package pl.edu.pwr.pp.services.reader;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

import pl.edu.pwr.pp.data.LoadedData;

class ImagePgmFileReader extends AbstractImageFileReader {

	/**
	 * Metoda czyta plik pgm, konwersuje na obraz i zapisuje w BufferedImage
	 * ImagePanelu.
	 * 
	 * @param fileName
	 *            nazwa pliku pgm
	 * @return
	 * @throws URISyntaxException
	 *             jeżeli plik nie istnieje
	 */
	@Override
	BufferedImage read(Path path) throws URISyntaxException {
		int columns = 0;
		int rows = 0;
		int[][] intensities = null;

		try (BufferedReader reader = Files.newBufferedReader(path)) {

			if (!reader.readLine().equals("P2")) {
				System.out.println("P2");
				throw new IOException();
			}

			if (!(reader.readLine().charAt(0) == '#')) {
				System.out.println("#");
				throw new IOException();
			}

			String columnsRows = reader.readLine();

			String[] splitted = columnsRows.split("\\s");

			columns = Integer.parseInt(splitted[0]);
			rows = Integer.parseInt(splitted[1]);

			if (!reader.readLine().equals("255")) {
				throw new IOException();
			}

			// inicjalizacja tablicy na odcienie szarości
			intensities = new int[rows][];

			for (int i = 0; i < rows; i++) {
				intensities[i] = new int[columns];
			}

			// kolejne linijki pliku pgm zawierają odcienie szarości kolejnych
			// pikseli rozdzielone spacjami
			String line = "";
			int currentRow = 0;
			int currentColumn = 0;
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				for (String val : line.split("\\s")) {
					if (val.isEmpty()) {
						continue;
					}
					intensities[currentRow][currentColumn] = Integer.parseInt(val);
					currentColumn++;

					if (currentColumn >= columns) {
						currentColumn = 0;
						currentRow++;
					}
					if (currentColumn > columns || currentRow > rows) {
						throw new RuntimeException();
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		LoadedData.loadedImage = intensities;

		return loadImageFromPgm(intensities);
	}

	private BufferedImage loadImageFromPgm(int[][] pgmImage) {

		int width = pgmImage[0].length;
		int height = pgmImage.length;
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		WritableRaster raster = image.getRaster();
		for (int y = 0; y < height; y++) {
			for (int x = 0; (x < width); x++) {
				raster.setSample(x, y, 0, pgmImage[y][x]);
			}
		}

		return image;
	}
}
