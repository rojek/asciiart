package pl.edu.pwr.pp.services;

import java.awt.Color;
import java.awt.image.BufferedImage;

class ImageToGreyscaleConverter {

	final static double RED_MULTIPLIER = 0.2989;
	final static double BLUE_MULTIPLIER = 0.1140;
	final static double GREEN_MULTIPLIER = 0.5870;

	int[][] convert(BufferedImage img) {

		if (img.getType() == BufferedImage.TYPE_BYTE_GRAY) {
			return null;
		}

		int height = img.getHeight();
		int width = img.getWidth();
		
		int[][] greyscaleImg = new int[height][width];
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				greyscaleImg[i][j] = toGreyscale(new Color(img.getRGB(j, i)));
			}
		}

		return greyscaleImg;
	}

	private int toGreyscale(Color color) {
		return (int) (color.getRed() * RED_MULTIPLIER 
					+ color.getBlue() * BLUE_MULTIPLIER
					+ color.getGreen() * GREEN_MULTIPLIER);
	}
}
