package pl.edu.pwr.pp.services;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.net.URISyntaxException;

import pl.edu.pwr.pp.data.LoadedData;
import pl.edu.pwr.pp.gui.MainAsciiArtWindow;
import pl.edu.pwr.pp.services.converter.ImageConverter;
import pl.edu.pwr.pp.services.reader.ImageFileReader;


public class ImageServicesDispatcher {

	private ImageResizer imageResizer = new ImageResizer();
	private ImageFileReader imageFileReader = new ImageFileReader();
	private ImageFileWriter imageFileWriter = new ImageFileWriter();
	private ImageConverter imageConverter = new ImageConverter();
	private ImageToGreyscaleConverter imageToGreyscaleConverter = new ImageToGreyscaleConverter();

	private static final ImageServicesDispatcher dispatcher = new ImageServicesDispatcher();

	public void resize() {
		BufferedImage resized = imageResizer.resize(MainAsciiArtWindow.getImage(), LoadedData.resizeMode);

		MainAsciiArtWindow.setImage(resized);
		saveIntensities(resized);
		MainAsciiArtWindow.repaintImagePanel();
		LoadedData.resized = true;
	}

	private void saveIntensities(BufferedImage resized) {

		int[][] intensities = new int[resized.getHeight()][resized.getWidth()];

		WritableRaster raster = resized.getRaster();

		for (int i = 0; i < resized.getWidth(); i++) {
			for (int j = 0; j < resized.getHeight(); j++) {
				intensities[j][i] = raster.getSample(i, j, 0);
			}
		}

		LoadedData.loadedImage = intensities;
	}

	public void readFile(String fileName) throws URISyntaxException, IOException {
		BufferedImage img = imageFileReader.readImage(fileName);
		int[][] converted = imageToGreyscaleConverter.convert(img);

		if (converted != null) {
			LoadedData.loadedImage = converted;
		}

		MainAsciiArtWindow.setImage(img);
	}

	public void writeFile(String fileName) throws IOException {
		
		char[][] ascii = imageConverter.intensitiesToAscii(LoadedData.loadedImage, LoadedData.quality);
		imageFileWriter.saveToTxtFile(ascii, fileName);
	}

	public static ImageServicesDispatcher getInstance() {

		return dispatcher;
	}
}
