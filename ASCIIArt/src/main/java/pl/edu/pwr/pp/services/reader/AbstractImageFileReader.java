package pl.edu.pwr.pp.services.reader;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;

abstract class AbstractImageFileReader {

	abstract BufferedImage read(Path path) throws IOException, URISyntaxException;
}
