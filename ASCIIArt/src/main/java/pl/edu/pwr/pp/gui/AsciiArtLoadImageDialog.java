package pl.edu.pwr.pp.gui;

import static java.awt.BorderLayout.CENTER;
import static pl.edu.pwr.pp.data.LoadMode.FILE;
import static pl.edu.pwr.pp.data.LoadMode.URL;
import static pl.edu.pwr.pp.data.LoadedData.loadMode;
import static pl.edu.pwr.pp.data.LoadedData.loadSuccesfull;
import static pl.edu.pwr.pp.gui.MainAsciiArtWindow.imagePanel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.io.File;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;

import org.apache.commons.io.FileUtils;

import pl.edu.pwr.pp.data.LoadedData;
import pl.edu.pwr.pp.services.ImageServicesDispatcher;

class AsciiArtLoadImageDialog extends JDialog {

	private static final long serialVersionUID = -7075570541424546630L;
	private final JPanel contentPanel = new JPanel();
	private FileTextField fileTextField = new FileTextField();
	private UrlTextField urlTextField;
	private ImageServicesDispatcher imageServicesDispatcher = ImageServicesDispatcher.getInstance();

	/**
	 * Launch the application.
	 */
	static void main(String[] args) {
		try {
			AsciiArtLoadImageDialog dialog = new AsciiArtLoadImageDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	AsciiArtLoadImageDialog() {
		setBounds(100, 100, 450, 220);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, CENTER);
		contentPanel.setLayout(null);
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 414, 127);
		contentPanel.add(panel);

		JRadioButton fileRadioButton = new JRadioButton("");
		JRadioButton urlRadioButton = new JRadioButton("");

		fileRadioButton.setBounds(20, 37, 21, 23);
		fileRadioButton.addActionListener(e -> {
			loadMode = FILE;
		});
		urlRadioButton.addActionListener(e -> {
			loadMode = URL;
		});

		JLabel lblZDysku = new JLabel("Z dysku");
		lblZDysku.setBounds(10, 11, 56, 14);
		panel.setLayout(null);
		panel.add(fileRadioButton);
		panel.add(lblZDysku);

		urlRadioButton.setBounds(23, 88, 21, 23);
		panel.add(urlRadioButton);

		JLabel lblZUrl = new JLabel("Z URL");
		lblZUrl.setBounds(13, 67, 42, 14);
		panel.add(lblZUrl);

		urlTextField = new UrlTextField(urlRadioButton);
		panel.add(new ChooseFileButton(fileRadioButton, fileTextField, contentPanel));
		panel.add(fileTextField);
		panel.add(urlTextField);
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		{
			JButton okButton = new JButton("OK");
			okButton.setActionCommand("OK");
			buttonPane.add(okButton);
			getRootPane().setDefaultButton(okButton);
			okButton.addActionListener(e -> {
				dispose();
				try {
					loadSuccesfull = true;
					switch (loadMode) {
					case FILE:
						loadFile();
						break;
					case URL:
						loadUrl();
						break;
					}

				} catch (Exception exc) {
					exc.printStackTrace();
					loadSuccesfull = false;
					showLoadFailedDialog();
				}

				if (loadSuccesfull) {
					MainAsciiArtWindow.saveImageButton.setEnabled(true);
					imagePanel.repaint();
				}
			});
			JButton cancelButton = new JButton("Anuluj");
			cancelButton.setActionCommand("Cancel");
			buttonPane.add(cancelButton);
			cancelButton.addActionListener(e -> dispose());
		}
	}

	private void showLoadFailedDialog() {
		new LoadFailedDialog().setVisible(true);
	}


	private void loadUrl() throws Exception {
		LoadedData.urlToLoad = urlTextField.getText();
		String[] splittedUrl = LoadedData.urlToLoad.split("\\.");
		File file = new File("temp." + splittedUrl[splittedUrl.length - 1]);
		FileUtils.copyURLToFile(new URL(LoadedData.urlToLoad), file);

		imageServicesDispatcher.readFile(file.getAbsolutePath());
	}

	private void loadFile() throws Exception {
		imageServicesDispatcher.readFile(LoadedData.fileNameToLoad);
	}
}
