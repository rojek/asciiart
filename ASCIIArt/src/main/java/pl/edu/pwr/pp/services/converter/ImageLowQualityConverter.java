package pl.edu.pwr.pp.services.converter;

class ImageLowQualityConverter extends AbstractImageQualityConverter {

	private static final String INTENSITY_2_ASCII_LOW = "@%#*+=-:. ";
	private static final int[] LIMITS_LOW = new int[] { 0, 26, 52, 77, 103, 128, 154, 180, 205, 231 };

	@Override
	char[][] convert(int[][] intensities) {

		char[][] retVal = new char[intensities.length][intensities[0].length];

		for (int i = 0; i < intensities.length; i++) {
			for (int j = 0; j < intensities[i].length; j++) {
				retVal[i][j] = intensityToAsciiLow(intensities[i][j]);
			}
		}

		return retVal;
	}

	/**
	 * Metoda zwraca znak odpowiadający danemu odcieniowi szarości. Odcienie
	 * szarości mogą przyjmować wartości z zakresu [0,255]. Zakres jest dzielony
	 * na równe przedziały, liczba przedziałów jest równa ilości znaków w
	 * {@value #INTENSITY_2_ASCII}. Zwracany znak jest znakiem dla przedziału,
	 * do którego należy zadany odcień szarości.
	 * 
	 * 
	 * @param intensity
	 *            odcień szarości w zakresie od 0 do 255
	 * @return znak odpowiadający zadanemu odcieniowi szarości
	 */
	char intensityToAsciiLow(int intensity) {

		for (int index = 9; index >= 0; index--) {
			if (intensity >= LIMITS_LOW[index]) {
				return INTENSITY_2_ASCII_LOW.charAt(index);
			}
		}

		return INTENSITY_2_ASCII_LOW.charAt(0);
	}

}
