package pl.edu.pwr.pp.gui;

import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

class LoadFailedDialog extends JDialog {

	LoadFailedDialog() {
		super();
		JPanel dialogPanel = new JPanel();
		setSize(300, 100);
		setTitle("Nie udało się załadować obrazka");
		dialogPanel.add(new JLabel("Nie udało się załadować obrazka"));
		JButton closeButton = new JButton("Zamknij okno");
		closeButton.addActionListener(event -> dispose());
		dialogPanel.add(closeButton);
		setLocation((int) Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2,
				(int) Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2);
		add(dialogPanel);
	}
}
