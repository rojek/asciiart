package pl.edu.pwr.pp.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

class ImageFileWriter {

	void saveToTxtFile(char[][] ascii, String fileName) throws IOException {
		
		ArrayList<String> output = new ArrayList<String>();


		for (char[] asciiLine : ascii) {
			String buffer = new String();
			for (char asciiChar : asciiLine) {
				buffer += asciiChar;
			}
			output.add(buffer);
		}
		Path outputPath = Paths.get(fileName);
		System.out.println("Saving " + fileName + " in " + outputPath.toAbsolutePath().toString());
		Files.write(outputPath, output);
	}
	
}
