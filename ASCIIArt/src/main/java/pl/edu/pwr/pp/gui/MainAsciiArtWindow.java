package pl.edu.pwr.pp.gui;

import java.awt.EventQueue;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainAsciiArtWindow {

	private JFrame frame;
	private JPanel controls;
	public static ImagePanel imagePanel;
	static JButton saveImageButton = new JButton("Zapisz do pliku");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MainAsciiArtWindow window = new MainAsciiArtWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainAsciiArtWindow() {
		initialize();
	}

	private void initialize() {
		frame = new MainWindowFrame();

		saveImageButton = new SaveImageButton(frame);
		imagePanel = new ImagePanel();

		controls = new JPanel();
		controls.setBounds(10, 11, 176, 239);
		controls.setLayout(null);
		controls.add(new ReadImageButton(frame));
		controls.add(saveImageButton);
		controls.add(new CloseButton(frame));
		controls.add(new AsciiQualityChangerButton());
		controls.add(new AsciiResizeChangerButton());

		frame.getContentPane().add(controls);

		frame.getContentPane().add(imagePanel);
	}

	public static void setImage(BufferedImage img) {
		imagePanel.setImage(img);
	}

	public static BufferedImage getImage() {
		return imagePanel.getImage();
	}

	public static void repaintImagePanel() {
		imagePanel.repaint();
	}
}
