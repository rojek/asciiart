package pl.edu.pwr.pp.services.converter;

class ImageHighQualityConverter extends AbstractImageQualityConverter {

	private static final String INTENSITY_2_ASCII_HIGH = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. ";

	@Override
	char[][] convert(int[][] intensities) {

		char[][] retVal = new char[intensities.length][intensities[0].length];

		for (int i = 0; i < intensities.length; i++) {
			for (int j = 0; j < intensities[i].length; j++) {
				retVal[i][j] = intensityToAsciiHigh(intensities[i][j]);
			}
		}

		return retVal;
	}

	char intensityToAsciiHigh(int intensity) {

		return INTENSITY_2_ASCII_HIGH.charAt((int) (intensity / 3.65));
	}

}
