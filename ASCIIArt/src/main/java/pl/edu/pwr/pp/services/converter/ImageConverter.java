package pl.edu.pwr.pp.services.converter;

import pl.edu.pwr.pp.data.Quality;

public class ImageConverter {

	private AbstractImageQualityConverter lowQualityConverter = new ImageLowQualityConverter();
	private AbstractImageQualityConverter highQualityConverter = new ImageHighQualityConverter();

	/**
	 * Metoda zwraca dwuwymiarową tablicę znaków ASCII mając dwuwymiarową
	 * tablicę odcieni szarości. Metoda iteruje po elementach tablicy odcieni
	 * szarości i dla każdego elementu wywołuje {@ref #intensityToAscii(int)}.
	 * 
	 * @param intensities
	 *            tablica odcieni szarości obrazu
	 * @return tablica znaków ASCII
	 */
	public char[][] intensitiesToAscii(int[][] intensities, Quality quality) {

		if (quality.equals(Quality.LOW)) {
			return lowQualityConverter.convert(intensities);
		} else {
			return highQualityConverter.convert(intensities);
		}
	}

}
